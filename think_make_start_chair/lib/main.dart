import 'package:flutter/material.dart';
import 'package:think_make_start_chair/pages/like_page.dart';
import 'package:think_make_start_chair/pages/root_page.dart';
import 'package:think_make_start_chair/utils/auth.dart';

import './pages/login_page.dart';
import './pages/button_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: RootPage(auth: Auth(),),
      //home: LikePage(),
    );
  }
}
