import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:think_make_start_chair/ui/dialog.dart' as dialog;


class User{
  String _id;
  int _height;
  int _weight;
  String _gender;

  int _frequency;
  int _interval;

  bool _exercise_notification;
  bool _move_notification;
  bool _water_notification;

  User(this._id, this._height, this._weight, this._gender, this._frequency,
      this._interval, this._exercise_notification, this._move_notification,
      this._water_notification);

  String get gender => _gender;

  int get weight => _weight;

  int get height => _height;

  String get id => _id;

  bool get water_notification => _water_notification;

  set water_notification(bool value) {
    _water_notification = value;
  }

  bool get move_notification => _move_notification;

  set move_notification(bool value) {
    _move_notification = value;
  }

  bool get exercise_notification => _exercise_notification;

  set exercise_notification(bool value) {
    _exercise_notification = value;
  }

  int get interval => _interval;

  set interval(int value) {
    _interval = value;
  }

  int get frequency => _frequency;

  set frequency(int value) {
    _frequency = value;
  }

  getDataDictionary() {
    var data = {
      "gender" : _gender,
      "height" : _height,
      "weight" : _weight,
      "frequency": _frequency,
      "interval": _interval,
      "move_notification": _move_notification,
      "exercise_notification": _exercise_notification,
      "water_notification": _water_notification
    };
    return data;
  }

  sendDataToDatabase(BuildContext context) {
    final collectionReference = Firestore.instance.collection("UserInformation");
    collectionReference.document(_id).setData(getDataDictionary()).whenComplete(() {
      print("Data send");
    }).catchError((e){
      var errorCode = e.code;
      var errorMessage = e.message;
      dialog.Dialog.information(context, "Data Store Message", errorMessage);
    });
  }

  updateData(BuildContext context){
    final collectionReference = Firestore.instance.collection("UserInformation");
    collectionReference.document(_id).updateData(getDataDictionary()).whenComplete(() {
      print("Data updated");
    }).catchError((e){
      var errorCode = e.code;
      var errorMessage = e.message;
      dialog.Dialog.information(context, "Data Store Message", errorMessage);
    });
  }
}

