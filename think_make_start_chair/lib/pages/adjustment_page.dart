import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:think_make_start_chair/ui/manual_adjustment_dialog.dart';
import 'dart:convert';

import 'package:think_make_start_chair/utils/User.dart';


class AdjustmentPage extends StatefulWidget{

  User _user;

  AdjustmentPage(this._user);

  @override
  State createState() {
    return AdjustmentPageState();
  }
}

class AdjustmentPageState extends State<AdjustmentPage>{

  void _sendData(String feed, int value) async {
    final response = await http.post(
        'https://io.adafruit.com/api/v2/SergeyM/feeds/'+ feed + '/data',
        headers: {
          'X-AIO-Key': '21e1ea1988a440298d27e8cf3b5faec7',
        },
        body: {
          'value': value.toString()
        }
    );
    print(json.decode(response.body));
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          //Image.asset(''),
          Icon(
            Icons.event_seat,
            size: 140.0,
          ),
          Container(
            margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 20.0),
            width: 200.0,
            height: 200.0,
            child: RawMaterialButton(
              fillColor: Colors.greenAccent,
              shape: CircleBorder(),
              elevation: 2.0,
              child: Text('GO',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0
                ),
              ),
              onPressed: (){
                _sendData('chair1.backrestheightmotor', 3);
                _sendData('chair1.sitheightmotor', 6);
              },
            )
          ),
          Container(
            width: 150.0,
            child: FlatButton(
              color: Colors.black,
              child: Text('Manual',
                style: TextStyle(
                  color: Colors.white
                ),
              ),
              onPressed: () {
                ManualDialog.showManualDialog(context, _sendData);
              },
            ),
          ),
          Container(
            width: 150.0,
            child: FlatButton(
              color: Colors.redAccent,
              child: Text('Cancel',
                style: TextStyle(

                ),
              ),
              onPressed: () {
                //TODO send to all
                _sendData('chair1.backrestheightmotor', -3);
                _sendData('chair1.sitheightmotor', -3);
              },
            ),
          ),
          Container(
            width: 150.0,
            child: FlatButton(
              color: Color(0xffdddddd),
              child: Text('Back',
                style: TextStyle(

                ),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
        ],
      ),
    );
  }
}
