import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


import 'package:think_make_start_chair/pages/adjustment_page.dart';
import 'package:think_make_start_chair/pages/recommender_page.dart';
import 'package:think_make_start_chair/pages/training_page.dart';
import 'package:think_make_start_chair/pages/like_page.dart';
import 'package:think_make_start_chair/utils/User.dart';
import 'package:think_make_start_chair/utils/auth.dart';

class ControlPage extends StatefulWidget{

  final BaseAuth auth;
  final VoidCallback onSignedOut;

  ControlPage({this.auth, this.onSignedOut});

  @override
  State createState() {
    return ControlPageState();
  }
}

class ControlPageState extends State<ControlPage> with WidgetsBindingObserver{

  User _user;

  void _fetchUser() async {
    final collectionReference = Firestore.instance.collection("UserInformation");
    String userID = await widget.auth.currentUser();
    collectionReference.document(userID).get().then((dataSnapshot) {
      if(dataSnapshot.exists){
        _user = User(
            userID,
            dataSnapshot.data['height'],
            dataSnapshot.data['weight'],
            dataSnapshot.data['gender'],
            dataSnapshot.data['frequency'],
            dataSnapshot.data['interval'],
            dataSnapshot.data['exercise_notification'],
            dataSnapshot.data['move_notification'],
            dataSnapshot.data['water_notification']
        );
      }
    });
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _fetchUser();
  }


  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print(state);
    // TODO
    /*
    if(state == AppLifecycleState.inactive){
      _user.updateData(context);
    }
    */
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Padding(
              padding: EdgeInsets.all(10),
              child: SizedBox.expand(
                child: FlatButton(
                  color: Color(0xff444444),
                  child: Text("Adjustment",
                    style: TextStyle(
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.white
                    ),
                  ),
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => AdjustmentPage(_user)));
                  },
                ),
              )
            ),
          ),
          Expanded(
            flex: 3,
            child: Padding(
              padding: EdgeInsets.all(10),
                child: SizedBox.expand(
                  child: FlatButton(
                    color: Color(0xff444444),
                    child: Text("Training",
                      style: TextStyle(
                          fontSize: 30.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white
                      ),
                    ),
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => TrainingPage(_user)));
                    },
                  ),
                )
            ),
          ),
          Expanded(
            flex: 3,
            child: Padding(
              padding: EdgeInsets.all(10),
                child: SizedBox.expand(
                  child: FlatButton(
                    color: Color(0xff444444),
                    child: Text("Smart Recommender",
                      style: TextStyle(
                          fontSize: 30.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white
                      ),
                    ),
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => RecommenderPage(_user)));
                    },
                  ),
                )
            ),
          ),
          RaisedButton(
            color: Colors.redAccent,
            child: Text("Logout"),
            onPressed: () {
              try{
                widget.auth.signOut().then((value) {
                  widget.onSignedOut();
                });
              }
              catch (e) {
                print(e);
              }
            },
          ),
          Container(
            width: 20,
            height: 20,
            child: FlatButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => LikePage()));
              },
              child: Container(),
            ),
          ),
        ],
      ),
    );
  }
}
