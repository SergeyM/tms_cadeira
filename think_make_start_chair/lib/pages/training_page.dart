import 'package:flutter/material.dart';
import 'package:think_make_start_chair/utils/User.dart';

class TrainingPage extends StatefulWidget{

  User _user;

  TrainingPage(this._user);

  @override
  State createState() {
    return TrainingPageState();
  }
}

class TrainingPageState extends State<TrainingPage>{

  double _frequencySliderValue;
  double _intervalSliderValue;


  @override
  void initState() {
    _frequencySliderValue = widget._user.frequency.toDouble();
    _intervalSliderValue = widget._user.interval.toDouble();
  }


  @override
  void dispose() {
    widget._user.updateData(context);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(vertical: 30),
            child: Text("Personal Trainingsplan",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 40.0
              ),
            ),
          ),
          Container(
            width: 300,
            margin: EdgeInsets.symmetric(vertical: 10),
            child: Column(
              children: <Widget>[
                Text("How often per day should your seat change its position?",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 20.0,
                      fontStyle: FontStyle.italic

                  ),
                ),

                Slider(
                  label: '${_frequencySliderValue.round()}',
                  onChanged: (double value) {
                    setState(() {
                      _frequencySliderValue = value;
                      widget._user.frequency = value.round();
                    });
                  },
                  divisions: 20,
                  value: _frequencySliderValue,
                  min: 0.0,
                  max: 20.0,
                )
              ],
            ),
          ),

          Container(
            width: 300,
            margin: EdgeInsets.symmetric(vertical: 10),
            child: Column(
              children: <Widget>[
                Text("In which interval should the position change? (in minutes)",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 20.0,
                      fontStyle: FontStyle.italic
                  ),
                ),

                Slider(
                  label: "${_intervalSliderValue.round()}",
                  onChanged: (double value) {
                    setState(() {
                      _intervalSliderValue = value;
                      widget._user.interval = value.round();
                    });
                  },
                  divisions: (120 / 15).floor() - 1,
                  value: _intervalSliderValue,
                  min: 15.0,
                  max: 120.0,
                )
              ],
            ),
          ),

          Container(
            width: 150.0,
            child: FlatButton(
              color: Color(0xffdddddd),
              child: Text('Back',
                style: TextStyle(

                ),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
        ],
      ),
    );
  }
}
