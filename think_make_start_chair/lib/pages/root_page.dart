import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:think_make_start_chair/pages/control_page.dart';
import 'package:think_make_start_chair/utils/auth.dart';

import 'login_page.dart';

class RootPage extends StatefulWidget{

  final BaseAuth auth;
  RootPage({this.auth});

  @override
  State createState() {
    return RootPageState();
  }
}

enum AuthStatus{
  notSignedIn,
  signedIn
}

class RootPageState extends State<RootPage> {

  AuthStatus _authStatus;

  @override
  void initState() {
    super.initState();
    widget.auth.currentUser().then((userId) {
      setState(() {
        _authStatus = userId == null ? AuthStatus.notSignedIn : AuthStatus.signedIn;
      });
    });
  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    switch(_authStatus) {
      case AuthStatus.notSignedIn:
        return LoginPage(
          auth: widget.auth,
          onSignedIn: () {
            setState(() {
              _authStatus = AuthStatus.signedIn;
            });
          },
        );
      case AuthStatus.signedIn:
        return ControlPage(
          auth: widget.auth,
          onSignedOut: () {
            setState(() {
              _authStatus = AuthStatus.notSignedIn;
            });
          },
        );
      default:
        return Material();
    }
  }
}

