import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'dart:async';
import 'dart:convert';
import 'dart:io';

class ButtonPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return ButtonPageState();
  }
}

class ButtonPageState extends State{

  TextEditingController _textEditingController = TextEditingController();

  @override
  void initState() {
    _getFeeds();
  }

  void _getFeeds() async {
    final response = await http.get(
      'https://io.adafruit.com/api/v2/SergeyM/feeds',
      headers: {
        'X-AIO-Key': '21e1ea1988a440298d27e8cf3b5faec7',
        'Content-Type': 'application/json'
      },
    );
    print(json.decode(response.body));
  }

  void _sendData(int value) async {
    final response = await http.post(
      'https://io.adafruit.com/api/v2/SergeyM/feeds/motor-control/data',
      headers: {
        'X-AIO-Key': '21e1ea1988a440298d27e8cf3b5faec7',
      },
      body: {
        'value': value.toString()
      }
    );
    print(json.decode(response.body));
  }

  @override
  Widget build(BuildContext context) {
    return  Material(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
                child: FlatButton(
                  child: Center(
                    child: Container(
                      child: Text("UP"),
                    ),
                  ),
                  onPressed: () {
                    _sendData(-1);
                  },
                )
            ),
            Expanded(
                child: FlatButton(
                  child: Center(
                    child: Container(
                      child: Text("HOLD"),
                    ),
                  ),
                  onPressed: () {
                    _sendData(-3);
                  },
                )
            ),
            Expanded(
                child: FlatButton(
                  child: Center(
                    child: Container(
                      child: Text("DOWN"),
                    ),
                  ),
                  onPressed: () {
                    _sendData(-2);
                  },
                )
            ),
            TextField(
              controller: _textEditingController,
              keyboardType: TextInputType.number,
            ),
            Expanded(
              child: FlatButton(
                child: Center(
                  child: Container(
                    child: Text("Send"),
                  ),
                ),
                onPressed: () {
                  var val = int.parse(_textEditingController.text);
                  if (val>100)
                    val = 100;
                  else if(val<0)
                    val = 0;
                  _sendData(val);
                  _textEditingController.clear();
                },
              ),
            ),
          ],
        )
    );
  }
}
