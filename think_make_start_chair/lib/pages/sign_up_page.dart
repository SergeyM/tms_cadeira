import 'package:flutter/material.dart';
import 'package:think_make_start_chair/utils/User.dart';
import 'package:think_make_start_chair/utils/auth.dart';
import 'package:think_make_start_chair/ui/dialog.dart' as dialog;
import 'package:cloud_firestore/cloud_firestore.dart';

class SignUpPage extends StatefulWidget{

  final BaseAuth auth;
  final VoidCallback onSignedUp;
  SignUpPage({this.auth, this.onSignedUp});

  @override
  State createState() {
    return SignUpPageState();
  }
}

class SignUpPageState extends State<SignUpPage>{

  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  final formKey = GlobalKey<FormState>();

  String _email;
  String _password;
  String _gender;
  double _heightSliderValue;
  double _weightSliderValue;

  @override
  void initState() {
    super.initState();
    _gender  = "male";
    _heightSliderValue = 0;
    _weightSliderValue = 0;
  }

  @override
  void dispose() {
    super.dispose();
  }

  void validateAndRegister() async {
    String userId;
    if (formKey.currentState.validate()){
      _email = _emailController.text.trim();
      _password = _passwordController.text;
      try{
        userId = await widget.auth.createUserWithEmailAndPassword(_email, _password);

        User user = User(userId, _heightSliderValue.round(), _weightSliderValue.round(), _gender, 0, 15, false, false, false);
        user.sendDataToDatabase(context);

        Navigator.pop(context);
        widget.onSignedUp();
      }
      catch (e) {
        var errorCode = e.code;
        var errorMessage = e.message;
          dialog.Dialog.information(context, "Sign up Message", errorMessage);
      }
    }
  }


  @override
  Widget build(BuildContext context) {
    return Material(
      child: Form(
        key: formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(20.0),
              child: Text("Sign Up",
                style: TextStyle(
                  fontSize: 50.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Container(
              width: 200.0,
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: TextField(
                controller: _emailController,
                textAlign: TextAlign.center,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: "Email",
                  filled: true,
                ),
              ),
            ),
            Container(
              width: 200.0,
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: TextField(
                obscureText: true,
                controller: _passwordController,
                textAlign: TextAlign.center,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: "Password",
                  filled: true,
                ),
              ),
            ),

            Container(
              padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 5.0),
              child: Text("Gender",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0
                ),
              ),
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Radio(
                  onChanged: (value){
                    setState(() {
                      _gender = value;
                    });
                  },
                  value: "male",
                  groupValue: _gender,
                ),
                Text("male"),
                Radio(
                  onChanged: (value){
                    setState(() {
                      _gender = value;
                    });
                  },
                  value: "female",
                  groupValue: _gender,
                ),
                Text("female"),
              ],
            ),

            Container(
              padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 5.0),
              child: Text("Body Height: " + _heightSliderValue.round().toString() + " cm",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0
                ),
              ),
            ),

            Container(
              width: 200.0,
              child: Slider(
                label: "${_heightSliderValue.round()}",
                divisions: 200,
                min: 0,
                max: 200,
                value: _heightSliderValue,
                onChanged: (value) {
                  setState(() {
                    _heightSliderValue = value;
                  });
                },
              ),
            ),

            Container(
              padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 5.0),
              child: Text("Body Weight: " + _weightSliderValue.round().toString() + " kg",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0
                ),
              ),
            ),

            Container(
              width: 200.0,
              child: Slider(
                label: "${_weightSliderValue.round()}",
                divisions: 200,
                min: 0,
                max: 200,
                value: _weightSliderValue,
                onChanged: (value) {
                  setState(() {
                    _weightSliderValue = value;
                  });
                },
              ),
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FlatButton(
                  child: Text("Back"),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                Container(
                  width: 20,
                ),
                RaisedButton(
                  child: Text("Sign up"),
                  onPressed: validateAndRegister,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
