import 'package:flutter/material.dart';

import 'package:think_make_start_chair/utils/User.dart';

class RecommenderPage extends StatefulWidget{

  User _user;

  RecommenderPage(this._user);

  @override
  State createState() {
    return RecommenderPageState();
  }
}

class RecommenderPageState extends State<RecommenderPage>{

  bool _waterNot;
  bool _moveNot;
  bool _exerciseNot;


  @override
  void initState() {
    super.initState();
    _waterNot = widget._user.water_notification;
    _moveNot = widget._user.move_notification;
    _exerciseNot = widget._user.exercise_notification;
  }

  @override
  void dispose() {
    widget._user.updateData(context);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Recommendation',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 45
            ),
          ),
          Container(
            width: 300,
            margin: EdgeInsets.symmetric(vertical: 20),
            child: Text('Thanks for passing by the Recommandation System!\nHere you can toggle the notifications for Water, Movement and Back exercises.',
              textAlign: TextAlign.center,
              style: TextStyle(

              ),
            ),
          ),

          Container(
            margin: EdgeInsets.symmetric(vertical: 10),
            child: RaisedButton(
              color: _waterNot ? Colors.greenAccent : Color(0xffdddddd),
              elevation: _waterNot ? 8 : 2,
              child: Text('Water notifications',
                style: TextStyle(

                ),
              ),
              onPressed: () {
                setState(() {
                  _waterNot = !_waterNot;
                  widget._user.water_notification = _waterNot;
                });
              },
            ),
          ),

          Container(
            margin: EdgeInsets.symmetric(vertical: 10),
            child: RaisedButton(
              color: _moveNot ? Colors.greenAccent : Color(0xffdddddd),
              elevation: _moveNot ? 8 : 2,
              child: Text('Movement notifications',
                style: TextStyle(

                ),
              ),
              onPressed: () {
                setState(() {
                  _moveNot = !_moveNot;
                  widget._user.move_notification = _moveNot;
                });
              },
            ),
          ),

          Container(
            margin: EdgeInsets.symmetric(vertical: 10),
            child: RaisedButton(
              color: _exerciseNot ? Colors.greenAccent : Color(0xffdddddd),
              elevation: _exerciseNot ? 8 : 2,
              child: Text('Back exercises',
                style: TextStyle(

                ),
              ),
              onPressed: () {
                setState(() {
                  _exerciseNot = !_exerciseNot;
                  widget._user.exercise_notification = _exerciseNot;
                });
              },
            ),
          ),

          Container(
            margin: EdgeInsets.symmetric(vertical: 30),
            width: 60.0,
            child: FlatButton(
              color: Color(0xffeeeeee),
              child: Text('Back',
                style: TextStyle(

                ),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
        ],
      ),
    );
  }
}
