import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:think_make_start_chair/ui/dialog.dart' as dialog;


class LikePage extends StatefulWidget{

  @override
  State createState() {
    return LikePageState();
  }
}

class LikePageState extends State{

  int _number = 0;

  updateLike(BuildContext context){
    final collectionReference = Firestore.instance.collection("Like");

    collectionReference.document("SFn49Ww0KXBRNQTFEMV3").get().then((dataSnapshot) {
      if(dataSnapshot.exists){
        _number = dataSnapshot.data['number'];
        print("Data received " + _number.toString());
        var data = {
          "number" : ++_number,
        };
        collectionReference.document("SFn49Ww0KXBRNQTFEMV3").updateData(data).whenComplete(() {
          print("Data updated " + data["number"].toString());
        }).catchError((e){
          var errorCode = e.code;
          var errorMessage = e.message;
          dialog.Dialog.information(context, "Data Store Message", errorMessage);
        });
      }
    }).catchError((e) {
      var errorCode = e.code;
      var errorMessage = e.message;
      dialog.Dialog.information(context, "Data Store Message", errorMessage);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
              // margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
              width: 300.0,
              height: 300.0,
              child: RawMaterialButton(
                fillColor: Colors.greenAccent,
                shape: CircleBorder(),
                elevation: 2.0,
                child: Icon(
                  Icons.thumb_up,
                  color: Colors.black,
                  size: 100,
                ),
                onPressed: (){
                  updateLike(context);
                },
              )
          ),
          Container(
            margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
            width: 20,
            height: 20,
            child: FlatButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Container(),
            ),
          ),
        ],
      ),
    );
  }
}
