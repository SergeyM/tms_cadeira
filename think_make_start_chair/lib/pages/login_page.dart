import 'package:flutter/material.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';
import 'package:think_make_start_chair/utils/auth.dart';
import 'package:think_make_start_chair/ui/dialog.dart' as dialog;

import './sign_up_page.dart';
import './like_page.dart';

class LoginPage extends StatefulWidget {
  final BaseAuth auth;
  final VoidCallback onSignedIn;
  LoginPage({this.auth, this.onSignedIn});

  @override
  State<LoginPage> createState() {
    return LoginPageState();
  }

}

class LoginPageState extends State<LoginPage>{
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  final formKey = GlobalKey<FormState>();

  int _keyboardListenerId;
  
  String _email;
  String _password;

  bool _showImage;

  @override
  void initState() {
    super.initState();

    _keyboardListenerId = KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        this.setState(() {
          _showImage = !visible;
        });
      },
    );

    _showImage = true;
  }

  void validateAndLogin() async {
    if (formKey.currentState.validate()){
      _email = _emailController.text.trim();
      _password = _passwordController.text;
      try{
        String userId = await widget.auth.signInWithEmailAndPassword(_email, _password);
        widget.onSignedIn();
      }
      catch (e) {
        var errorCode = e.code;
        var errorMessage = e.message;
        dialog.Dialog.information(context, "Login Error", errorMessage);
      }
    }
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    KeyboardVisibilityNotification().removeListener(_keyboardListenerId);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _showImage ?
          Image.asset(
            'assets/logo.png',
          ) :
          Container(
            padding: EdgeInsets.symmetric(vertical: 20.0),
            child: Text("CADEIRA",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontStyle: FontStyle.italic,
                fontSize: 20.0,
                letterSpacing: 3.0
              ),
            ),
          ),
          Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                Container(
                  width: 300.0,
                  padding: EdgeInsets.symmetric(vertical: 10.0),
                  child: TextFormField(
                    validator: (value) => value.isEmpty ? 'Email can not be empty!' : null,
                    controller: _emailController,
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                      icon: Icon(
                        Icons.mail,
                      ),
                      border: OutlineInputBorder(),
                      labelText: "E-Mail",
                      filled: true,
                    ),
                  ),
                ),
                Container(
                  width: 300.0,
                  padding: EdgeInsets.symmetric(vertical: 10.0),
                  child: TextFormField(
                    validator: (value) => value.isEmpty ? 'Password can not be empty!' : null,
                    controller: _passwordController,
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                      icon: Icon(
                        Icons.lock,
                      ),
                      border: OutlineInputBorder(),
                      labelText: "Password",
                      filled: true,
                    ),
                    obscureText: true,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FlatButton(
                      child: Text("Cancel"),
                      onPressed: () {
                        _password = "";
                        _email = "";
                        _emailController.clear();
                        _passwordController.clear();
                      },
                    ),
                    Container(
                      width: 20,
                    ),
                    RaisedButton(
                      child: Text("Sign in"),
                      onPressed: validateAndLogin,
                    )
                  ],
                ),
              ],
            ),
          ),

          FlatButton(
            child: Text("Sign up",
              style: TextStyle(
                color: Colors.grey
              ),
            ),
            onPressed: () {
              formKey.currentState.reset();
              _emailController.clear();
              _passwordController.clear();
              Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => SignUpPage(auth: widget.auth, onSignedUp: widget.onSignedIn,))
              );
            },
          ),

          Container(
            width: 20,
            height: 20,
            child: FlatButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => LikePage()));
              },
              child: Container(),
            ),
          ),
        ],
      ),
    );
  }
}
