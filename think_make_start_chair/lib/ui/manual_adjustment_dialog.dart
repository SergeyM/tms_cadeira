import 'package:flutter/material.dart';

class ManualDialog {

  static showManualDialog(BuildContext context, Function(String, int) function){
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: Text("Manual Adjustment",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 30.0,
              fontWeight: FontWeight.bold
            ),
          ),
          children: <Widget>[
            Container(
              margin: EdgeInsets.symmetric(vertical: 10.0),
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 8.0),
                    child: Text("Sit height",
                      style: TextStyle(
                          fontSize: 20.0
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          margin: EdgeInsets.symmetric(horizontal: 10.0),
                          width: 40.0,
                          height: 40.0,
                          child: RawMaterialButton(
                            fillColor: Colors.black,
                            shape: CircleBorder(),
                            elevation: 2.0,
                            child: Icon(
                              Icons.arrow_upward,
                              color: Colors.white,
                            ),
                            onPressed: (){
                              function('chair1.sitheightmotor', -1);
                            },
                          )
                      ),
                      Container(
                          margin: EdgeInsets.symmetric(horizontal: 10.0),
                          width: 40.0,
                          height: 40.0,
                          child: RawMaterialButton(
                            fillColor: Colors.black,
                            shape: CircleBorder(),
                            elevation: 2.0,
                            child: Icon(
                              Icons.arrow_downward,
                              color: Colors.white,
                            ),
                            onPressed: (){
                              function('chair1.sitheightmotor', -2);
                            },
                          )
                      ),
                      Container(
                          margin: EdgeInsets.symmetric(horizontal: 10.0),
                          width: 40.0,
                          height: 40.0,
                          child: RawMaterialButton(
                            fillColor: Colors.black,
                            shape: CircleBorder(),
                            elevation: 2.0,
                            child: Icon(
                              Icons.stop,
                              color: Colors.white,
                            ),
                            onPressed: (){
                              function('chair1.sitheightmotor', -3);
                            },
                          )
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 8.0),
                    child: Text("Backrest height",
                      style: TextStyle(
                          fontSize: 20.0
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          margin: EdgeInsets.symmetric(horizontal: 10.0),
                          width: 40.0,
                          height: 40.0,
                          child: RawMaterialButton(
                            fillColor: Colors.black,
                            shape: CircleBorder(),
                            elevation: 2.0,
                            child: Icon(
                              Icons.arrow_upward,
                              color: Colors.white,
                            ),
                            onPressed: (){
                              function('chair1.backrestheightmotor', -1);
                            },
                          )
                      ),
                      Container(
                          margin: EdgeInsets.symmetric(horizontal: 10.0),
                          width: 40.0,
                          height: 40.0,
                          child: RawMaterialButton(
                            fillColor: Colors.black,
                            shape: CircleBorder(),
                            elevation: 2.0,
                            child: Icon(
                              Icons.arrow_downward,
                              color: Colors.white,
                            ),
                            onPressed: (){
                              function('chair1.backrestheightmotor', -2);
                            },
                          )
                      ),
                      Container(
                          margin: EdgeInsets.symmetric(horizontal: 10.0),
                          width: 40.0,
                          height: 40.0,
                          child: RawMaterialButton(
                            fillColor: Colors.black,
                            shape: CircleBorder(),
                            elevation: 2.0,
                            child: Icon(
                              Icons.stop,
                              color: Colors.white,
                            ),
                            onPressed: (){
                              function('chair1.backrestheightmotor', -3);
                            },
                          )
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 20.0),
              child: FloatingActionButton(
                child: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
                backgroundColor: Color(0xffdddddd),
              ),
            ),
          ],
        );
      },
    );
  }
}
