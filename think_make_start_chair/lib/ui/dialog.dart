import 'package:flutter/material.dart';

class Dialog{
  static information(BuildContext context, String title, String description){
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(description)
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: new Text("Ok"),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }


  // dialog.waiting(...)
  // await Future.delayed(Duration(seconds: 2))
  // Navigator.pop(context);
  static waiting(BuildContext context, String title, String description){
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(description)
              ],
            ),
          ),
        );
      },
    );
  }


  static _confirmResult(bool isYes, BuildContext context) {
    if(isYes){
      Navigator.pop(context);
    }
    else{
      Navigator.pop(context);
    }
  }

  static confirm(BuildContext context, String title, String description){
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(description)
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: new Text("Cancel"),
              onPressed: _confirmResult(false, context),
            ),
            FlatButton(
              child: new Text("Yes"),
              onPressed: _confirmResult(true, context),
            ),
          ],
        );
      },
    );
  }
}

