const int buttonPin = 5;     
//const int ledPin =  5;     

//Variables
int buttonState = 0;

int IN1 = 11;
int IN2 = 12;

#define ON   0
#define OFF  1

void setup() {
  //Input or output?
  //pinMode(ledPin, OUTPUT);      
  pinMode(buttonPin, INPUT_PULLUP);   

  relay_init();
  Serial.begin(9600);
}

void loop(){
  //Read button state (pressed or not pressed?)
  buttonState = digitalRead(buttonPin);
  Serial.print(buttonState);
  //If button pressed...
  if (buttonState == HIGH) { 
    relay_SetStatus(OFF, ON);//turn on RELAY_2
  }
  else {
    relay_SetStatus(OFF, OFF);//turn on RELAY_2
  }

  // relay_SetStatus(ON, OFF);//turn on RELAY_1
  
  
  

}

void relay_init(void)//initialize the relay
{
  //set all the relays OUTPUT
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  relay_SetStatus(OFF, OFF); //turn off all the relay
}

void relay_SetStatus( unsigned char status_1,  unsigned char status_2)
{
  digitalWrite(IN1, status_1);
  digitalWrite(IN2, status_2);
}
