#include "config.h"

const int ledPin = 13;

const int buttonPin1 = 20;     
const int buttonPin2 = 21;     

const int bridge1Pin1 = 10;     
const int bridge1Pin2 = 11; 

const int bridge2Pin1 = 5;     
const int bridge2Pin2 = 6;

const int motorPosPin = A0;

int buttonState1;
int buttonState2;

int backMotorPos;
int backMotorEndPos;

long timeReceivedBackMotor;
long backMotorTime;

long timeReceivedSitMotor;
long sitMotorTime;

int backMotorState;
int sitMotorState;

// int status = WL_IDLE_STATUS;     // the WiFi radio's status

AdafruitIO_Feed *backrest_feed = io.feed("chair1.backrestheightmotor");
AdafruitIO_Feed *sitHeight_feed = io.feed("chair1.sitheightmotor");


void setup() {
  pinMode(bridge1Pin1, OUTPUT);      
  pinMode(bridge1Pin2, OUTPUT);  
  pinMode(bridge2Pin1, OUTPUT);      
  pinMode(bridge2Pin2, OUTPUT); 
   
  pinMode(buttonPin1, INPUT_PULLUP);   
  pinMode(buttonPin2, INPUT_PULLUP); 
    
  pinMode(motorPosPin, INPUT);
  pinMode(ledPin, OUTPUT);

  backMotorPos = 0; 
  backMotorEndPos = 0;

  timeReceivedBackMotor = 0;
  backMotorTime = -1;

  timeReceivedSitMotor = 0;
  sitMotorTime = -1;  
  
  backMotorState = 0;
  sitMotorState = 0;
  
  buttonState1 = 0;
  buttonState2 = 0;

  digitalWrite(ledPin, LOW);
  digitalWrite(bridge1Pin1, 255);
  digitalWrite(bridge1Pin2, 255);
  digitalWrite(bridge2Pin1, 255);
  digitalWrite(bridge2Pin2, 255);
  
  Serial.begin(9600);
  // while(! Serial);

  Serial.print("Connecting to Adafruit IO");
  io.connect();
  backrest_feed->onMessage(handle_backrest);
  sitHeight_feed->onMessage(handle_sitHeight);
  bool an = false;
  while(io.status() < AIO_CONNECTED) {
    Serial.print(".");
    digitalWrite(ledPin, an ? HIGH : LOW);
    delay(500);
  }
  Serial.println();
  Serial.println(io.statusText());
  Serial.println();
  printCurrentNet();
  printWiFiData();
  Serial.println();

  backrest_feed->save(-3);
  sitHeight_feed->save(-3);

  backrest_feed->get();
  sitHeight_feed->get();
  
  digitalWrite(ledPin, HIGH);

/*
  WiFi.setPins(8,7,4,2);
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present");
    // don't continue:
    while (true);
  }
  while ( status != WL_CONNECTED) {
    Serial.print("Attempting to connect to WPA SSID: ");
    Serial.println(WIFI_SSID);
    // Connect to WPA/WPA2 network:
    status = WiFi.begin(WIFI_SSID, WIFI_PASS);

    // wait 10 seconds for connection:
    delay(10000);
  }
  Serial.print("You're connected to the network");
  printCurrentNet();
  printWiFiData();
*/
}

void loop() {
  io.run();
  
  buttonState1 = digitalRead(buttonPin1);
  buttonState2 = digitalRead(buttonPin2);
  if(buttonState1 == HIGH || buttonState2 == HIGH){
    backMotorState = 0;
    sitMotorState = 0;
    control_bridge_by_button();
  }
  else{
    control_bridge_by_app();  
  }



/*
  Serial.print("Button 1: ");
  Serial.println(buttonState1);
  Serial.print("Button 2: ");
  Serial.println(buttonState2);
*/

  backMotorPos = analogRead(motorPosPin);

  /*
  Serial.println();
  Serial.print("Motor Position: ");
  Serial.println(motorPosition);
  Serial.print("Motor Position Goal: ");
  Serial.println(motorControlPos);
  Serial.print("Motor Control State: ");
  Serial.println(motorControlState);
  Serial.println();
  */
}

void control_bridge_by_button(){
  if(buttonState1 == HIGH){
    digitalWrite(bridge1Pin1, 255);
    digitalWrite(bridge1Pin2, 0);
    
    digitalWrite(bridge2Pin1, 255);
    digitalWrite(bridge2Pin2, 0);
  }
  else if(buttonState2 == HIGH) {   
    digitalWrite(bridge1Pin1, 0);
    digitalWrite(bridge1Pin2, 255);   

    digitalWrite(bridge2Pin1, 0);
    digitalWrite(bridge2Pin2, 255);   
  }
}

void control_bridge_by_app(){
  // Stop Back Motor if its near the end

  /*
  if(backMotorEndPos == -1 && ((backMotorState == 2 && backMotorPos < 100) || (backMotorState == 1 && backMotorPos > 900))){
    backMotorState = 0;
  }
  */

  // BACK MOTOR CONTROL
  if(backMotorState == 1){
    /*
    if(backMotorEndPos == -1 || backMotorPos < backMotorEndPos){
      digitalWrite(bridge1Pin1, 255);
      digitalWrite(bridge1Pin2, 0);
    }
    */
    if(backMotorTime == -1 || millis() - timeReceivedBackMotor < backMotorTime){
      digitalWrite(bridge1Pin1, 255);
      digitalWrite(bridge1Pin2, 0);
    }
    else {
      backMotorState = 0;
    }
  }
  else if(backMotorState == 2) {
    /*
    if(backMotorEndPos == -1 || backMotorPos > backMotorEndPos){
      digitalWrite(bridge1Pin1, 0);
      digitalWrite(bridge1Pin2, 255);
    }
    */
    if(backMotorTime == -1 || millis() - timeReceivedBackMotor < backMotorTime){
      digitalWrite(bridge1Pin1, 0);
      digitalWrite(bridge1Pin2, 255);
    }
    else {
      backMotorState = 0;
    }
  }
  else {
    digitalWrite(bridge1Pin1, 255);
    digitalWrite(bridge1Pin2, 255); 
    backMotorEndPos = -1;
    backMotorTime = -1;
  }  

  // SIT MOTOR CONTROL
  if(sitMotorState == 1){
    if(sitMotorTime == -1 || millis() - timeReceivedSitMotor < sitMotorTime) {
      digitalWrite(bridge2Pin1, 255);
      digitalWrite(bridge2Pin2, 0);
    }
    else {
      sitMotorState = 0;
    }
  }
  else if(sitMotorState == 2) {
    if(sitMotorTime == -1 || millis() - timeReceivedSitMotor < sitMotorTime) {
      digitalWrite(bridge2Pin1, 0);
      digitalWrite(bridge2Pin2, 255);
    } 
    else {
      sitMotorState = 0;
    }
  }
  else {
    digitalWrite(bridge2Pin1, 255);
    digitalWrite(bridge2Pin2, 255); 
    sitMotorTime = -1;
  }  
}

void handleMessage(AdafruitIO_Data *data) {

  Serial.print("received <- ");
  Serial.println(data->value());

}


void setStateAndPos(int value, int motor){
  // BACK MOTOR STATE AND POSITION SETTING
  if(motor == 1){
    if(value < 0){
      if(value == -3){
        backMotorState = 0;
      }
      else {
        backMotorState = -value;
      }
      backMotorEndPos = -1;
      backMotorTime = -1;
      return;
    }

    /*
    backMotorEndPos = map(value, 0, 100, 100, 900);
    if(backMotorPos < backMotorEndPos){
      backMotorState = 1;  
    }
    else if(backMotorPos > backMotorEndPos){
      backMotorState = 2;
    }
    else {
      backMotorState = 0;
    }  
    */
    backMotorState = random(1,3);
    backMotorTime = value * 1000;
    timeReceivedBackMotor = millis();
  }
  // SIT MOTOR STATE SETTING
  else if(motor == 2){
    if(value < 0){
      if(value == -3){
        sitMotorState = 0;
      }
      else {
        sitMotorState = -value;
      }
      sitMotorTime = -1;
      return;
    }

    sitMotorState = random(1,3);
    sitMotorTime = value * 1000;
    timeReceivedSitMotor = millis();
  }  
}

void handle_backrest(AdafruitIO_Data *data){
  char* data_value = data->value();
  int val = strtol(data_value, NULL, 10);

  Serial.print("Backrest Motor: received <- ");
  Serial.println(val);

  setStateAndPos(val, 1);
}

void handle_sitHeight(AdafruitIO_Data *data){
  char* data_value = data->value();
  int val = strtol(data_value, NULL, 10);

  Serial.print("Sit Height Motor: received <- ");
  Serial.println(val);

  setStateAndPos(val, 2);
}

//--------------------------------------------------------------------------- WIFI Information print

void printWiFiData() {
  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  Serial.println(ip);

  // print your MAC address:
  byte mac[6];
  WiFi.macAddress(mac);
  Serial.print("MAC address: ");
  printMacAddress(mac);

}

void printCurrentNet() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print the MAC address of the router you're attached to:
  byte bssid[6];
  WiFi.BSSID(bssid);
  Serial.print("BSSID: ");
  printMacAddress(bssid);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.println(rssi);

  // print the encryption type:
  byte encryption = WiFi.encryptionType();
  Serial.print("Encryption Type:");
  Serial.println(encryption, HEX);
  Serial.println();
}

void printMacAddress(byte mac[]) {
  for (int i = 5; i >= 0; i--) {
    if (mac[i] < 16) {
      Serial.print("0");
    }
    Serial.print(mac[i], HEX);
    if (i > 0) {
      Serial.print(":");
    }
  }
  Serial.println();
}
